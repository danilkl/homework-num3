//
//  UnaryOperation.h
//  Calculator
//
//  Created by Danil on 04.11.16.
//  Copyright © 2016 Danil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnaryOperation : NSObject

- (double)perform:(double)value;

@end
