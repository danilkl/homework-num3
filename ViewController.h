//
//  ViewController.h
//  Calculator
//
//  Created by Danil on 02.11.16.
//  Copyright © 2016 Danil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculatorModel.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *numbericLabel;
@property (nonatomic, assign, getter=isNumberInputing) BOOL numberInputing;
@property (nonatomic, strong) CalculatorModel *model;
@end

