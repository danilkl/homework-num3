//
//  ViewController.m
//  Calculator
//
//  Created by Danil on 02.11.16.
//  Copyright © 2016 Danil. All rights reserved.
//

#import "ViewController.h"





@implementation ViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    self.model = [[CalculatorModel alloc] init];
}


- (IBAction)pressDigit:(UIButton *)sender {
    if(self.isNumberInputing){
        if(![sender.currentTitle isEqualToString:@","] || ![self.numbericLabel.text containsString:@","]){
            self.numbericLabel.text = [self.numbericLabel.text stringByAppendingString:sender.currentTitle];
        }
    }
    else{
        if([sender.currentTitle isEqualToString:@","]) {
            self.numbericLabel.text = [NSString stringWithFormat:@"0,"];
        }
        else {
            self.numbericLabel.text = sender.currentTitle;
        }
        self.numberInputing = YES;
    }
    
}
- (IBAction)perfomOperation:(UIButton *)sender {
    self.numberInputing = NO;
}


@end
