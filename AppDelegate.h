//
//  AppDelegate.h
//  Calculator
//
//  Created by Danil on 02.11.16.
//  Copyright © 2016 Danil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

