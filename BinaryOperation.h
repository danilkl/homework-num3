//
//  BinaryOperation.h
//  Calculator
//
//  Created by Danil on 04.11.16.
//  Copyright © 2016 Danil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BinaryOperation : NSObject
@property (nonatomic, assign) double firstArgument;
- (double)performWithSecondArgument:(double)secondArgument;
@end
